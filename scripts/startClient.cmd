@echo off
@echo Setup System

set JAVA_HOME=C:\nova-encores\java\jdk1.7.0_80_x64
set JAVA_BIN=%JAVA_HOME%\bin\java.exe

set JAVA_OPTS=
set CLASSPATH=%cd%\\libs\\*;
set LOG4J_CONFIGURATION=%cd%\\logs\\log4j_client.xml
set JAVA_OPTS=%JAVA_OPTS% -Dlog4j.configuration=file:%LOG4J_CONFIGURATION%


@echo Setting JAVA_OPTS

@echo Starting Migration
set CMD=%JAVA_BIN% -cp %CLASSPATH% %JAVA_OPTS% bosko.test.Main
echo %CMD%
%CMD%

pause
