package bosko.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class T09RPPOId implements Serializable {

    private static final long serialVersionUID = 4742695205806105390L;

    private T09RPREId t09rpreId;
    private Integer rech_postnr;

    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public Integer getRech_postnr() {
        return rech_postnr;
    }

    public void setRech_postnr(final Integer aRech_postnr) {
        rech_postnr = aRech_postnr;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (rech_postnr == null ? 0
                : rech_postnr.hashCode());
        result = prime * result + (t09rpreId == null ? 0
                : t09rpreId.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPPOId other = (T09RPPOId) obj;
        if (rech_postnr == null) {
            if (other.rech_postnr != null) {
                return false;
            }
        } else if (!rech_postnr.equals(other.rech_postnr)) {
            return false;
        }
        if (t09rpreId == null) {
            if (other.t09rpreId != null) {
                return false;
            }
        } else if (!t09rpreId.equals(other.t09rpreId)) {
            return false;
        }
        return true;
    }
}
