package bosko.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class T09RPDEId implements Serializable {

    private static final long serialVersionUID = -6652882413732868212L;

    private T09RPREId t09rpreId;
    private Integer dnnr;
    private Integer fanr;

    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public Integer getDnnr() {
        return dnnr;
    }

    public void setDnnr(final Integer aDnnr) {
        dnnr = aDnnr;
    }

    public Integer getFanr() {
        return fanr;
    }

    public void setFanr(final Integer aFanr) {
        fanr = aFanr;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (dnnr == null ? 0
                : dnnr.hashCode());
        result = prime * result + (fanr == null ? 0
                : fanr.hashCode());
        result = prime * result + (t09rpreId == null ? 0
                : t09rpreId.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPDEId other = (T09RPDEId) obj;
        if (dnnr == null) {
            if (other.dnnr != null) {
                return false;
            }
        } else if (!dnnr.equals(other.dnnr)) {
            return false;
        }
        if (fanr == null) {
            if (other.fanr != null) {
                return false;
            }
        } else if (!fanr.equals(other.fanr)) {
            return false;
        }
        if (t09rpreId == null) {
            if (other.t09rpreId != null) {
                return false;
            }
        } else if (!t09rpreId.equals(other.t09rpreId)) {
            return false;
        }
        return true;
    }

}
