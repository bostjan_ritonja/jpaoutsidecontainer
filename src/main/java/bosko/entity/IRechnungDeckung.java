package bosko.entity;

public interface IRechnungDeckung {

    public Integer getDnnr();

    public Integer getFanr();

    public Integer getRechnr();
}
