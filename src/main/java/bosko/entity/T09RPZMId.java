package bosko.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class T09RPZMId implements Serializable {

    private static final long serialVersionUID = -4520041016308887094L;

    private T09RPREId t09rpreId;
    private Integer zahl_lfdnr;

    public T09RPZMId() {
        super();
    }

    public T09RPZMId(final String aString) {
        final String[] splits = aString.split("\\s+");
        t09rpreId = new T09RPREId();
        t09rpreId.setRechnr(Integer.valueOf(splits[0]));
        t09rpreId.setRech_vorg_lfdnr(Integer.valueOf(splits[1]));
        zahl_lfdnr = Integer.valueOf(splits[2]);
    }

    public T09RPZMId(final Integer rechnr, final Integer rech_vorg_lfdnr, final Integer zahl_lfdnr) {
        t09rpreId = new T09RPREId();
        t09rpreId.setRechnr(rechnr);
        t09rpreId.setRech_vorg_lfdnr(rech_vorg_lfdnr);
        this.zahl_lfdnr = zahl_lfdnr;
    }

    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public Integer getZahl_lfdnr() {
        return zahl_lfdnr;
    }

    public void setZahl_lfdnr(final Integer aZahl_lfdnr) {
        zahl_lfdnr = aZahl_lfdnr;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (t09rpreId == null ? 0
                : t09rpreId.hashCode());
        result = prime * result + (zahl_lfdnr == null ? 0
                : zahl_lfdnr.hashCode());
        return result;
    }

    @Override
    @Transient
    public String toString() {
        return t09rpreId.getIdentifier() + " " + getZahl_lfdnr().toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPZMId other = (T09RPZMId) obj;
        if (t09rpreId == null) {
            if (other.t09rpreId != null) {
                return false;
            }
        } else if (!t09rpreId.equals(other.t09rpreId)) {
            return false;
        }
        if (zahl_lfdnr == null) {
            if (other.zahl_lfdnr != null) {
                return false;
            }
        } else if (!zahl_lfdnr.equals(other.zahl_lfdnr)) {
            return false;
        }
        return true;
    }
}
