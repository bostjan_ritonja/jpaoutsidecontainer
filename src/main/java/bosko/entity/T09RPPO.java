package bosko.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "T09RPPO", schema = "AGA")
public class T09RPPO implements Serializable {

    private static final long serialVersionUID = -7616660057222069870L;

    private T09RPPOId t09rppoId;

    private Integer updz;
    private String rech_post_art;
    private Integer rech_gesch_vorfnr;
    private String mand_ber;
    private String soll_haben_kz;
    private Date faell_dat;
    private BigDecimal entg_dm;
    private Integer deck_art;
    private Date meld_mm_jahr;
    private Integer sachb_aend;
    private Date aend_dat;
    private String umsatz_aus_kz;
    private Integer nomination_kz;
    private String waehr_schl;

    private T09RPRE t09rpre;

    public T09RPPO() {
    }

    public T09RPPO(final T09RPRE aT09rpre, final Integer aRech_postnr) {
        setT09rppoId(new T09RPPOId());
        getT09rppoId().setRech_postnr(aRech_postnr);
        aT09rpre.addT09rppos(this);
    }

    @MapsId("t09rpreId")
    @JoinColumns({ @JoinColumn(name = "rechnr"), @JoinColumn(name = "rech_vorg_lfdnr") })
    @ManyToOne
    public T09RPRE getT09rpre() {
        return t09rpre;
    }

    public void setT09rpre(final T09RPRE aT09rpre) {
        t09rpre = aT09rpre;
    }

    @EmbeddedId
    public T09RPPOId getT09rppoId() {
        return t09rppoId;
    }

    public void setT09rppoId(final T09RPPOId aT09rppoId) {
        t09rppoId = aT09rppoId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    public String getRech_post_art() {
        return rech_post_art;
    }

    public void setRech_post_art(final String aRech_post_art) {
        rech_post_art = aRech_post_art;
    }

    public Integer getRech_gesch_vorfnr() {
        return rech_gesch_vorfnr;
    }

    public void setRech_gesch_vorfnr(final Integer aRech_gesch_vorfnr) {
        rech_gesch_vorfnr = aRech_gesch_vorfnr;
    }

    public String getMand_ber() {
        return mand_ber;
    }

    public void setMand_ber(final String aMand_ber) {
        mand_ber = aMand_ber;
    }

    public String getSoll_haben_kz() {
        return soll_haben_kz;
    }

    public void setSoll_haben_kz(final String aSoll_haben_kz) {
        soll_haben_kz = aSoll_haben_kz;
    }

    public Date getFaell_dat() {
        return faell_dat;
    }

    public void setFaell_dat(final Date aFaell_dat) {
        faell_dat = aFaell_dat;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getEntg_dm() {
        return entg_dm;
    }

    public void setEntg_dm(final BigDecimal aEntg_dm) {
        entg_dm = aEntg_dm;
    }

    public Integer getDeck_art() {
        return deck_art;
    }

    public void setDeck_art(final Integer aDeck_art) {
        deck_art = aDeck_art;
    }

    public Date getMeld_mm_jahr() {
        return meld_mm_jahr;
    }

    public void setMeld_mm_jahr(final Date aMeld_mm_jahr) {
        meld_mm_jahr = aMeld_mm_jahr;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    public String getUmsatz_aus_kz() {
        return umsatz_aus_kz;
    }

    public void setUmsatz_aus_kz(final String aUmsatz_aus_kz) {
        umsatz_aus_kz = aUmsatz_aus_kz;
    }

    public Integer getNomination_kz() {
        return nomination_kz;
    }

    public void setNomination_kz(final Integer aNomination_kz) {
        nomination_kz = aNomination_kz;
    }

    public String getWaehr_schl() {
        return waehr_schl;
    }

    public void setWaehr_schl(final String aWaehr_schl) {
        waehr_schl = aWaehr_schl;
    }

    // public String mapSAPVerwendungsartEKG(
    // final String aDeckForm,
    // final Integer aDeckungApgnr,
    // final Integer aDeckungFanr) throws MigrationException {
    // final String sAPVerwendungsart =
    // SAPVerwendungsartMapper.mapSAPVerwendungsart(getMand_ber(), aDeckForm, getDeck_art(),
    // getRech_post_art(), aDeckungApgnr, aDeckungFanr);
    // if (sAPVerwendungsart != null) {
    // return sAPVerwendungsart;
    // }
    // throw new MigrationException(MigrationMessages.MAPPING_SAP_VERWENDUNGSART_FEHLER.format(
    // String.valueOf(getT09rppoId().getRech_postnr()), getRech_post_art(), String.valueOf(getDeck_art()),
    // getMand_ber(), aDeckForm, String.valueOf(aDeckungApgnr), String.valueOf(aDeckungFanr)));
    // }
}
