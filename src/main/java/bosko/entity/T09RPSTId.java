package bosko.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class T09RPSTId implements Serializable {

    private static final long serialVersionUID = 4892234602424524393L;

    private T09RPREId t09rpreId;
    private String rech_status_art;

    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public String getRech_status_art() {
        return rech_status_art;
    }

    public void setRech_status_art(final String aRech_status_art) {
        rech_status_art = aRech_status_art;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (rech_status_art == null ? 0
                : rech_status_art.hashCode());
        result = prime * result + (t09rpreId == null ? 0
                : t09rpreId.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPSTId other = (T09RPSTId) obj;
        if (rech_status_art == null) {
            if (other.rech_status_art != null) {
                return false;
            }
        } else if (!rech_status_art.equals(other.rech_status_art)) {
            return false;
        }
        if (t09rpreId == null) {
            if (other.t09rpreId != null) {
                return false;
            }
        } else if (!t09rpreId.equals(other.t09rpreId)) {
            return false;
        }
        return true;
    }

}
