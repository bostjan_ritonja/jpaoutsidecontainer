package bosko.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class T09RPMAId implements Serializable {

    private static final long serialVersionUID = -7478814668674647188L;

    private T09RPREId t09rpreId;
    private Date mahn_dat;
    private Integer mahn_stufe;

    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public Date getMahn_dat() {
        return mahn_dat;
    }

    public void setMahn_dat(final Date aMahn_dat) {
        mahn_dat = aMahn_dat;
    }

    public Integer getMahn_stufe() {
        return mahn_stufe;
    }

    public void setMahn_stufe(final Integer aMahn_stufe) {
        mahn_stufe = aMahn_stufe;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (mahn_dat == null ? 0
                : mahn_dat.hashCode());
        result = prime * result + (mahn_stufe == null ? 0
                : mahn_stufe.hashCode());
        result = prime * result + (t09rpreId == null ? 0
                : t09rpreId.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPMAId other = (T09RPMAId) obj;
        if (mahn_dat == null) {
            if (other.mahn_dat != null) {
                return false;
            }
        } else if (!mahn_dat.equals(other.mahn_dat)) {
            return false;
        }
        if (mahn_stufe == null) {
            if (other.mahn_stufe != null) {
                return false;
            }
        } else if (!mahn_stufe.equals(other.mahn_stufe)) {
            return false;
        }
        if (t09rpreId == null) {
            if (other.t09rpreId != null) {
                return false;
            }
        } else if (!t09rpreId.equals(other.t09rpreId)) {
            return false;
        }
        return true;
    }

}
