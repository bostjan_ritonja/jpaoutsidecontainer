package bosko.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "T09RPDE", schema = "AGA")
public class T09RPDE implements Serializable, IRechnungDeckung {

    private static final long serialVersionUID = -326145045757359829L;

    private T09RPDEId t09rpdeId;

    private Integer updz;
    private Integer deck_version;
    private Integer apgnr;
    private String sap_schuld_kz;
    private String mahn_kz;
    private Integer sachb_aend;
    private Date aend_dat;

    private T09RPRE t09rpre;

    public T09RPDE() {

    }

    public T09RPDE(final T09RPRE aT09rpre, final Integer aDnnr, final Integer aFanr) {
        setT09rpdeId(new T09RPDEId());
        getT09rpdeId().setDnnr(aDnnr);
        getT09rpdeId().setFanr(aFanr);
        aT09rpre.addT09rpdes(this);
    }

    @MapsId("t09rpreId")
    @JoinColumns({ @JoinColumn(name = "rechnr"), @JoinColumn(name = "rech_vorg_lfdnr") })
    @ManyToOne
    public T09RPRE getT09rpre() {
        return t09rpre;
    }

    public void setT09rpre(final T09RPRE aT09rpre) {
        t09rpre = aT09rpre;
    }

    @EmbeddedId
    public T09RPDEId getT09rpdeId() {
        return t09rpdeId;
    }

    public void setT09rpdeId(final T09RPDEId aT09rpdeId) {
        t09rpdeId = aT09rpdeId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    public Integer getDeck_version() {
        return deck_version;
    }

    public void setDeck_version(final Integer aDeck_version) {
        deck_version = aDeck_version;
    }

    public Integer getApgnr() {
        return apgnr;
    }

    public void setApgnr(final Integer aApgnr) {
        apgnr = aApgnr;
    }

    public String getSap_schuld_kz() {
        return sap_schuld_kz;
    }

    public void setSap_schuld_kz(final String aSap_schuld_kz) {
        sap_schuld_kz = aSap_schuld_kz;
    }

    public String getMahn_kz() {
        return mahn_kz;
    }

    public void setMahn_kz(final String aMahn_kz) {
        mahn_kz = aMahn_kz;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    @Transient
    public Integer getDnnr() {
        return getT09rpdeId().getDnnr();
    }

    @Transient
    public Integer getFanr() {
        return getT09rpdeId().getFanr();
    }

    @Transient
    public Integer getRechnr() {
        return getT09rpdeId().getT09rpreId().getRechnr();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (getDnnr() == null ? 0
                : getDnnr().hashCode());
        result = prime * result + (getFanr() == null ? 0
                : getFanr().hashCode());
        result = prime * result + (getRechnr() == null ? 0
                : getRechnr().hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof IRechnungDeckung)) {
            return false;
        }
        final IRechnungDeckung other = (IRechnungDeckung) obj;
        if (getDnnr() == null) {
            if (other.getDnnr() != null) {
                return false;
            }
        } else if (!getDnnr().equals(other.getDnnr())) {
            return false;
        }
        if (getFanr() == null) {
            if (other.getFanr() != null) {
                return false;
            }
        } else if (!getFanr().equals(other.getFanr())) {
            return false;
        }
        if (getRechnr() == null) {
            if (other.getRechnr() != null) {
                return false;
            }
        } else if (!getRechnr().equals(other.getRechnr())) {
            return false;
        }
        return true;
    }

    @Transient
    public boolean isNotForApg() {
        return getApgnr() == null || getApgnr() != null && getApgnr() == 0;
    }

    @Transient
    public boolean isKeyForVersionValid() {
        return getDnnr() != null && getFanr() != null && getDeck_version() != null && getDnnr() > 0 && getFanr() > 0
                && getDeck_version() > 0;
    }
}
