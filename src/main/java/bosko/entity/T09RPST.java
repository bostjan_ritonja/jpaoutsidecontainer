package bosko.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "T09RPST", schema = "AGA")
public class T09RPST implements Serializable {

    private static final long serialVersionUID = -9164995407459219166L;

    private T09RPSTId t09rpstId;

    private Integer updz;
    private Date rech_status_dat;
    private Integer sachb_freigabe;
    private String pgm_aend;
    private Integer sachb_aend;
    private Date aend_dat;

    private T09RPRE t09rpre;

    public T09RPST() {

    }

    public T09RPST(final T09RPRE aT09rpre, final String aRech_status_art) {
        setT09rpstId(new T09RPSTId());
        getT09rpstId().setRech_status_art(aRech_status_art);
        aT09rpre.addT09rpsts(this);
    }

    @MapsId("t09rpreId")
    @JoinColumns({ @JoinColumn(name = "rechnr"), @JoinColumn(name = "rech_vorg_lfdnr") })
    @ManyToOne
    public T09RPRE getT09rpre() {
        return t09rpre;
    }

    public void setT09rpre(final T09RPRE aT09rpre) {
        t09rpre = aT09rpre;
    }

    @EmbeddedId
    public T09RPSTId getT09rpstId() {
        return t09rpstId;
    }

    public void setT09rpstId(final T09RPSTId aT09rpstId) {
        t09rpstId = aT09rpstId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    public Date getRech_status_dat() {
        return rech_status_dat;
    }

    public void setRech_status_dat(final Date aRech_status_dat) {
        rech_status_dat = aRech_status_dat;
    }

    public Integer getSachb_freigabe() {
        return sachb_freigabe;
    }

    public void setSachb_freigabe(final Integer aSachb_freigabe) {
        sachb_freigabe = aSachb_freigabe;
    }

    public String getPgm_aend() {
        return pgm_aend;
    }

    public void setPgm_aend(final String aPgm_aend) {
        pgm_aend = aPgm_aend;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    @Transient
    public boolean isKorrektur() {
        if ("20".equals(getT09rpstId().getRech_status_art())) {
            return true;
        }
        return false;
    }

    @Transient
    public boolean is09AndBefore(final Date aMigratedHistoryFrom) {
        if ("09".equals(getT09rpstId().getRech_status_art()) && getRech_status_dat().before(aMigratedHistoryFrom)) {
            return true;
        }
        return false;
    }
}
