package bosko.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class T09RPREId implements Serializable {

    private static final long serialVersionUID = -4865062574181232611L;

    private Integer rechnr;
    private Integer rech_vorg_lfdnr;

    public T09RPREId() {
    }

    public T09RPREId(final Integer aRechnr, final Integer aRech_vorg_lfdnr) {
        super();
        setRechnr(aRechnr);
        setRech_vorg_lfdnr(aRech_vorg_lfdnr);
    }

    @Override
    public String toString() {
        return getRechnr().toString() + " " + getRech_vorg_lfdnr().toString();
    }

    @Transient
    public String getIdentifier() {
        return getRechnr().toString() + " " + getRech_vorg_lfdnr().toString();
    }

    public Integer getRechnr() {
        return rechnr;
    }

    public void setRechnr(final Integer aRechnr) {
        rechnr = aRechnr;
    }

    public Integer getRech_vorg_lfdnr() {
        return rech_vorg_lfdnr;
    }

    public void setRech_vorg_lfdnr(final Integer aRech_vorg_lfdnr) {
        rech_vorg_lfdnr = aRech_vorg_lfdnr;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (rech_vorg_lfdnr == null ? 0
                : rech_vorg_lfdnr.hashCode());
        result = prime * result + (rechnr == null ? 0
                : rechnr.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final T09RPREId other = (T09RPREId) obj;
        if (rech_vorg_lfdnr == null) {
            if (other.rech_vorg_lfdnr != null) {
                return false;
            }
        } else if (!rech_vorg_lfdnr.equals(other.rech_vorg_lfdnr)) {
            return false;
        }
        if (rechnr == null) {
            if (other.rechnr != null) {
                return false;
            }
        } else if (!rechnr.equals(other.rechnr)) {
            return false;
        }
        return true;
    }
}
