package bosko.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.openjpa.persistence.OpenJPAPersistence;
import org.apache.openjpa.persistence.OpenJPAQuery;

@NamedQueries({
    @NamedQuery(
            name = T09RPRE.NamedQueries.EKG_ALL_T09RPRE_IDS,
            query = "select r.t09rpreId from T09RPRE r where r.rechnr_man is null or (r.rechnr_man not like 'KAP%' AND r.rechnr_man not like 'UFK%')"),
    @NamedQuery(name = T09RPRE.NamedQueries.DIA_ALL_T09RPRE_IDS,
            query = "select r.t09rpreId from T09RPRE r where r.rechnr_man like 'KAP%' or r.rechnr_man like 'UFK%'"),
    @NamedQuery(name = T09RPRE.NamedQueries.LOAD_FROM_LIST,
            query = "select r from T09RPRE r where r.t09rpreId in :list") })
@NamedNativeQueries({
    @NamedNativeQuery(name = T09RPRE.NamedNativeQueries.ALL_WITHOUT_T09RPST,
            query = "select distinct re.RECHNR, re.RECH_VORG_LFDNR from aga.t09rpre re "
                    + "left join aga.t09rpst st on re.RECHNR = st.RECHNR and re.RECH_VORG_LFDNR = st.RECH_VORG_LFDNR "
                    + "where st.RECHNR is null"),
    @NamedNativeQuery(name = T09RPRE.NamedNativeQueries.ALL_WITHOUT_T09RPDE,
            query = "select distinct re.RECHNR, re.RECH_VORG_LFDNR from aga.t09rpre re "
                    + "left join aga.t09rpde de on re.RECHNR = de.RECHNR and re.RECH_VORG_LFDNR = de.RECH_VORG_LFDNR "
                    + "where de.RECHNR is null"),
    @NamedNativeQuery(
            name = T09RPRE.NamedNativeQueries.EKG_WITHOUT_T09ERVS,
            query = "select distinct re.RECHNR, re.RECH_VORG_LFDNR from aga.t09rpre re "
                    + "left join aga.t09rpde de on re.RECHNR = de.RECHNR and re.RECH_VORG_LFDNR = de.RECH_VORG_LFDNR "
                    + "left join aga.t09ervs vs on de.DNNR = vs.DNNR and de.FANR = vs.FANR and de.DECK_VERSION = vs.DECK_VERSION "
                    + "where de.RECHNR is not null and (re.rechnr_man is null or (re.rechnr_man not like 'KAP%' AND re.rechnr_man not like 'UFK%')) and de.apgnr is null "
                    + "and vs.DNNR is null") })
@Entity
@Table(name = "T09RPRE", schema = "AGA")
public class T09RPRE implements Serializable {

    private static final long serialVersionUID = 6579050946839833914L;

    private T09RPREId t09rpreId;

    private Integer updz;
    private Integer rech_gesch_vorfnr;
    private String rech_art;
    private String rechnr_man;
    private Date rech_dat;
    private String mahnverf_kz;
    private Date mahnverf_dat;
    private Integer sachb_aend;
    private Date aend_dat;
    private String rech_vorl_kz;

    private List<T09RPPO> t09rppos = new ArrayList<T09RPPO>();
    private List<T09RPMA> t09rpmas = new ArrayList<T09RPMA>();
    private List<T09RPDE> t09rpdes = new ArrayList<T09RPDE>();
    private List<T09RPZM> t09rpzms = new ArrayList<T09RPZM>();
    private List<T09RPST> t09rpsts = new ArrayList<T09RPST>();

    public static interface NamedQueries {
        public static final String EKG_ALL_T09RPRE_IDS = "T09RPRE.ekg_all_t09rpre_ids";
        public static final String DIA_ALL_T09RPRE_IDS = "T09RPRE.dia_all_t09rpre_ids";
        public static final String LOAD_FROM_LIST = "T09RPRE.load_from_list";
    }

    public static interface NamedNativeQueries {
        public static final String EKG_WITHOUT_T09ERVS = "T09RPRE.EKG_WITHOUT_T09ERVS";
        public static final String ALL_WITHOUT_T09RPDE = "T09RPRE.ALL_WITHOUT_T09RPD";
        public static final String ALL_WITHOUT_T09RPST = "T09RPRE.ALL_WITHOUT_T09RPST";
    }

    public T09RPRE() {

    }

    public T09RPRE(final Integer aRechnr, final Integer aRech_vorg_lfdnr) {
        setT09rpreId(new T09RPREId());
        getT09rpreId().setRechnr(aRechnr);
        getT09rpreId().setRech_vorg_lfdnr(aRech_vorg_lfdnr);
    }

    @OneToMany(targetEntity = T09RPPO.class, mappedBy = "t09rpre", cascade = CascadeType.ALL)
    @OrderBy("t09rppoId.rech_postnr ASC")
    public List<T09RPPO> getT09rppos() {
        return t09rppos;
    }

    public void setT09rppos(final List<T09RPPO> aT09rppos) {
        t09rppos = aT09rppos;
    }

    @Transient
    public void addT09rppos(final T09RPPO aT09rppo) {
        getT09rppos().add(aT09rppo);
        aT09rppo.setT09rpre(this);
        aT09rppo.getT09rppoId().setT09rpreId(getT09rpreId());
    }

    @OneToMany(targetEntity = T09RPMA.class, mappedBy = "t09rpre", cascade = CascadeType.ALL)
    @OrderBy("t09rpmaId.mahn_dat ASC")
    public List<T09RPMA> getT09rpmas() {
        return t09rpmas;
    }

    public void setT09rpmas(final List<T09RPMA> aT09rpmas) {
        t09rpmas = aT09rpmas;
    }

    @Transient
    public void addT09rpmas(final T09RPMA aT09rpma) {
        getT09rpmas().add(aT09rpma);
        aT09rpma.setT09rpre(this);
        aT09rpma.getT09rpmaId().setT09rpreId(getT09rpreId());
    }

    @OneToMany(targetEntity = T09RPDE.class, mappedBy = "t09rpre", cascade = CascadeType.ALL)
    public List<T09RPDE> getT09rpdes() {
        return t09rpdes;
    }

    public void setT09rpdes(final List<T09RPDE> aT09rpdes) {
        t09rpdes = aT09rpdes;
    }

    @Transient
    public void addT09rpdes(final T09RPDE aT09rpde) {
        getT09rpdes().add(aT09rpde);
        aT09rpde.setT09rpre(this);
        aT09rpde.getT09rpdeId().setT09rpreId(getT09rpreId());
    }

    @OneToMany(targetEntity = T09RPZM.class, mappedBy = "t09rpre", cascade = CascadeType.ALL)
    public List<T09RPZM> getT09rpzms() {
        return t09rpzms;
    }

    public void setT09rpzms(final List<T09RPZM> aT09rpzms) {
        t09rpzms = aT09rpzms;
    }

    @Transient
    public void addT09rpzms(final T09RPZM aT09rpzm) {
        getT09rpzms().add(aT09rpzm);
        aT09rpzm.setT09rpre(this);
        aT09rpzm.getT09rpzmId().setT09rpreId(getT09rpreId());
    }

    @OneToMany(targetEntity = T09RPST.class, mappedBy = "t09rpre", cascade = CascadeType.ALL)
    @OrderBy("rech_status_dat ASC, t09rpstId.rech_status_art ASC")
    public List<T09RPST> getT09rpsts() {
        return t09rpsts;
    }

    public void setT09rpsts(final List<T09RPST> aT09rpsts) {
        t09rpsts = aT09rpsts;
    }

    @Transient
    public void addT09rpsts(final T09RPST aT09rpst) {
        getT09rpsts().add(aT09rpst);
        aT09rpst.setT09rpre(this);
        aT09rpst.getT09rpstId().setT09rpreId(getT09rpreId());
    }

    @EmbeddedId
    public T09RPREId getT09rpreId() {
        return t09rpreId;
    }

    public void setT09rpreId(final T09RPREId aT09rpreId) {
        t09rpreId = aT09rpreId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    public Integer getRech_gesch_vorfnr() {
        return rech_gesch_vorfnr;
    }

    public void setRech_gesch_vorfnr(final Integer aRech_gesch_vorfnr) {
        rech_gesch_vorfnr = aRech_gesch_vorfnr;
    }

    public String getRech_art() {
        return rech_art;
    }

    public void setRech_art(final String aRech_art) {
        rech_art = aRech_art;
    }

    public String getRechnr_man() {
        return rechnr_man;
    }

    public void setRechnr_man(final String aRechnr_man) {
        rechnr_man = aRechnr_man;
    }

    public Date getRech_dat() {
        return rech_dat;
    }

    public void setRech_dat(final Date aRech_dat) {
        rech_dat = aRech_dat;
    }

    public String getMahnverf_kz() {
        return mahnverf_kz;
    }

    public void setMahnverf_kz(final String aMahnverf_kz) {
        mahnverf_kz = aMahnverf_kz;
    }

    public Date getMahnverf_dat() {
        return mahnverf_dat;
    }

    public void setMahnverf_dat(final Date aMahnverf_dat) {
        mahnverf_dat = aMahnverf_dat;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    public String getRech_vorl_kz() {
        return rech_vorl_kz;
    }

    public void setRech_vorl_kz(final String aRech_vorl_kz) {
        rech_vorl_kz = aRech_vorl_kz;
    }

    @SuppressWarnings("unchecked")
    @Transient
    public static List<T09RPRE> loadAllIn(final EntityManager entityManager, final List<T09RPREId> keys) {
        if (keys.isEmpty()) {
            return new ArrayList<T09RPRE>();
        }
        final Query query = entityManager.createNamedQuery(T09RPRE.NamedQueries.LOAD_FROM_LIST);
        query.setParameter("list", keys);
        @SuppressWarnings("rawtypes")
        final OpenJPAQuery kq = OpenJPAPersistence.cast(query);
        kq.getFetchPlan().addField(T09RPRE.class, "t09rppos");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpmas");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpdes");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpsts");
        return query.getResultList();
    }

    @Transient
    public boolean isForAPG() {
        for (final T09RPDE each : getT09rpdes()) {
            if (!each.isNotForApg()) {
                return true;
            }
        }
        return false;
    }

    public static List<T09RPRE> loadAllInWithCriteriaBuildedr(
            final EntityManager aEkgEntityManager,
            final List<T09RPREId> aKeys) {
        final CriteriaBuilder criteriaBuilder = aEkgEntityManager.getCriteriaBuilder();
        final CriteriaQuery<T09RPRE> criteriaQuery = criteriaBuilder.createQuery(T09RPRE.class);
        final Root<T09RPRE> rpre = criteriaQuery.from(T09RPRE.class);
        criteriaQuery.select(rpre);
        final Predicate buildConditions = buildConditions(criteriaBuilder, rpre, aKeys);
        if (buildConditions == null) {
            return new ArrayList<T09RPRE>();
        }
        criteriaQuery.where(buildConditions);
        final TypedQuery<T09RPRE> query = aEkgEntityManager.createQuery(criteriaQuery);

        @SuppressWarnings("rawtypes")
        final OpenJPAQuery kq = OpenJPAPersistence.cast(query);
        kq.getFetchPlan().addField(T09RPRE.class, "t09rppos");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpmas");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpdes");
        kq.getFetchPlan().addField(T09RPRE.class, "t09rpsts");
        return query.getResultList();
    }

    private static Predicate buildConditions(
            final CriteriaBuilder criteriaBuilder,
            final Root<T09RPRE> rpre,
            final List<T09RPREId> keys) {
        Predicate result = null;
        for (final T09RPREId key : keys) {
            if (result == null) {
                result = getCriteria(criteriaBuilder, rpre, key);
            } else {
                result = criteriaBuilder.or(result, getCriteria(criteriaBuilder, rpre, key));
            }
        }
        return result;
    }

    private static Predicate getCriteria(
            final CriteriaBuilder criteriaBuilder,
            final Root<T09RPRE> rpre,
            final T09RPREId key) {
        return criteriaBuilder.and(criteriaBuilder.equal(rpre.get("t09rpreId").get("rechnr"), key.getRechnr()),
                criteriaBuilder.equal(rpre.get("t09rpreId").get("rech_vorg_lfdnr"), key.getRech_vorg_lfdnr()));
    }

}
