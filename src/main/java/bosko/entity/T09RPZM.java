package bosko.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Transient;

@NamedQueries({
    @NamedQuery(
            name = T09RPZM.NamedQueries.ALL_KEYS,
            query = "select zm.t09rpzmId from T09RPZM zm order by zm.t09rpzmId.t09rpreId.rechnr, zm.t09rpzmId.t09rpreId.rech_vorg_lfdnr, zm.t09rpzmId.zahl_lfdnr "),
    @NamedQuery(name = T09RPZM.NamedQueries.LOAD_FROM_LIST,
            query = "select zm from T09RPZM zm where zm.t09rpzmId in :list"),
    @NamedQuery(name = T09RPZM.NamedQueries.SUM_SCHON_BEZAHLT_FOR_RECHNR,
            query = "select zm.t09rpzmId.t09rpreId.rechnr, sum(zm.zahl_eing_dm) " //
                    + "from T09RPZM zm " //
                    + "where zm.t09rpzmId.t09rpreId.rechnr in :list "//
                    + "group by zm.t09rpzmId.t09rpreId.rechnr") })
@Entity
@Table(name = "T09RPZM", schema = "AGA")
public class T09RPZM implements Serializable {

    public static String LOAD_ALL_IN_KEYS_PARAMETER = "KEYS";

    private static final long serialVersionUID = 2384206896283870147L;

    private T09RPZMId t09rpzmId;

    private Integer updz;
    private String zahl_basis_kz;
    private BigDecimal zahl_eing_dm;
    private Date zahl_eing_dat;
    private Integer sachb_aend;
    private Date aend_dat;
    private String mand_ber;
    private Integer nomination_kz;
    private String waehr_schl;

    private T09RPRE t09rpre;

    public static interface NamedQueries {
        public static final String ALL_KEYS = "T09RPZM.all_Keys";
        public static final String LOAD_FROM_LIST = "T09RPZM.load_from_list";
        public static final String SUM_SCHON_BEZAHLT_FOR_RECHNR = "T09RPZM.SumSchonBezahltForRechnr";
    }

    public T09RPZM() {
    }

    public T09RPZM(final T09RPRE aT09rpre, final Integer aZahl_lfdnr) {
        setT09rpzmId(new T09RPZMId());
        getT09rpzmId().setZahl_lfdnr(aZahl_lfdnr);
        aT09rpre.addT09rpzms(this);
    }

    @MapsId("t09rpreId")
    @JoinColumns({ @JoinColumn(name = "rechnr"), @JoinColumn(name = "rech_vorg_lfdnr") })
    @ManyToOne
    public T09RPRE getT09rpre() {
        return t09rpre;
    }

    public void setT09rpre(final T09RPRE aT09rpre) {
        t09rpre = aT09rpre;
    }

    @EmbeddedId
    public T09RPZMId getT09rpzmId() {
        return t09rpzmId;
    }

    public void setT09rpzmId(final T09RPZMId aT09rpzmId) {
        t09rpzmId = aT09rpzmId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    public String getZahl_basis_kz() {
        return zahl_basis_kz;
    }

    public void setZahl_basis_kz(final String aZahl_basis_kz) {
        zahl_basis_kz = aZahl_basis_kz;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getZahl_eing_dm() {
        return zahl_eing_dm;
    }

    public void setZahl_eing_dm(final BigDecimal aZahl_eing_dm) {
        zahl_eing_dm = aZahl_eing_dm;
    }

    public Date getZahl_eing_dat() {
        return zahl_eing_dat;
    }

    public void setZahl_eing_dat(final Date aZahl_eing_dat) {
        zahl_eing_dat = aZahl_eing_dat;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    public String getMand_ber() {
        return mand_ber;
    }

    public void setMand_ber(final String aMand_ber) {
        mand_ber = aMand_ber;
    }

    public Integer getNomination_kz() {
        return nomination_kz;
    }

    public void setNomination_kz(final Integer aNomination_kz) {
        nomination_kz = aNomination_kz;
    }

    public String getWaehr_schl() {
        return waehr_schl;
    }

    public void setWaehr_schl(final String aWaehr_schl) {
        waehr_schl = aWaehr_schl;
    }

    @SuppressWarnings("unchecked")
    @Transient
    public static List<Object[]> loadAllIn(final EntityManager entityManager, final Map<String, Object> map) {
        final List<T09RPZMId> keys = (List<T09RPZMId>) map.get(LOAD_ALL_IN_KEYS_PARAMETER);
        if (keys.isEmpty()) {
            return new ArrayList<Object[]>();
        }

        final StringBuilder queryBuffer = new StringBuilder(1000);
        queryBuffer.append("select zm.RECHNR, zm.ZAHL_EING_DM, zm.ZAHL_EING_DAT, zm.MAND_BER, zm.ZAHL_BASIS_KZ, apg.rechnr, zm.WAEHR_SCHL, zm.RECH_VORG_LFDNR, zm.ZAHL_LFDNR from ");
        queryBuffer.append("AGA");
        queryBuffer.append(".t09rpzm zm ");
        queryBuffer.append("left join (select de.RECHNR, de.RECH_VORG_LFDNR from ");
        queryBuffer.append("AGA");
        queryBuffer.append(".t09rpde de ");
        queryBuffer.append("where de.APGNR is not null and de.APGNR <> 0 ");
        queryBuffer.append("group by de.RECHNR, de.RECH_VORG_LFDNR) apg ");
        queryBuffer.append("on zm.RECHNR = apg.RECHNR and zm.RECH_VORG_LFDNR = apg.RECH_VORG_LFDNR ");
        queryBuffer.append("where ");

        for (int i = 0; i < keys.size(); i++) {
            if (i != 0) {
                queryBuffer.append(" or ");
            }
            queryBuffer.append("(zm.RECHNR = ? and zm.RECH_VORG_LFDNR = ? and zm.ZAHL_LFDNR = ?)");
        }

        final Query query = entityManager.createNativeQuery(queryBuffer.toString());
        for (int i = 0; i < keys.size(); i++) {
            final T09RPZMId key = keys.get(i);
            query.setParameter(i * 3 + 1, key.getT09rpreId().getRechnr());
            query.setParameter(i * 3 + 2, key.getT09rpreId().getRech_vorg_lfdnr());
            query.setParameter(i * 3 + 3, key.getZahl_lfdnr());
        }

        return query.getResultList();

        // final Query query = entityManager.createNamedQuery(T09RPZM.NamedQueries.LOAD_FROM_LIST);
        // query.setParameter("list", keys);
        // final OpenJPAQuery kq = OpenJPAPersistence.cast(query);
        // kq.getFetchPlan().addField(T09RPZM.class, "t09rpre");
        // kq.getFetchPlan().addField(T09RPRE.class, "t09rpdes");
        // return query.getResultList();
    }

    @Transient
    public boolean isForAPG() {
        if (getT09rpre() == null) {
            return false;
        }
        return getT09rpre().isForAPG();
    }

    // @Transient
    // public static Map<Integer, BigDecimal> sumSchonBezahltForRechnr(
    // final IMigrationDataAccessObject anEkgDao,
    // final Set<Integer> anAllRechnr) {
    // if (anAllRechnr.isEmpty()) {
    // return new HashMap<Integer, BigDecimal>();
    // }
    // final Map<String, Object> parameters = new HashMap<String, Object>();
    // parameters.put("list", anAllRechnr);
    // final List<Object[]> queryResultList =
    // anEkgDao.getResultListFromNamedQuery(T09RPZM.NamedQueries.SUM_SCHON_BEZAHLT_FOR_RECHNR, parameters);
    // final Map<Integer, BigDecimal> result = new HashMap<Integer, BigDecimal>();
    // for (final Object[] queryResultObject : queryResultList) {
    // result.put((Integer) queryResultObject[0], (BigDecimal) queryResultObject[1]);
    // }
    // return result;
    // }
}
