package bosko.entity;

public class SAPVerwendungsartMapper {

    public static String mapSAPVerwendungsart(
            final String aMandBer,
            final String aDeckForm,
            final Integer aDeckArt,
            final String aRechPostenArt,
            final Integer aDeckungApgnr,
            final Integer aDeckungFanr) {

        if ("01".equals(aRechPostenArt) || "02".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_01_02(aMandBer, aDeckForm, aDeckArt, aDeckungApgnr, aDeckungFanr);
        }
        if ("03".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_03(aMandBer);
        }
        if ("04".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_04(aMandBer, aDeckArt, aDeckForm);
        }
        if ("05".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_05(aMandBer);
        }
        if ("06".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_06(aMandBer);
        }
        if ("07".equals(aRechPostenArt)) {
            return "141";
        }
        if ("08".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_08(aMandBer);
        }
        if ("13".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_13(aMandBer, aDeckForm);
        }
        if (is_postArt_either_10_11_12_14_15_16_17(aRechPostenArt)) {
            return mapSAPVerwendungsart_10_to_17_No_13(aMandBer);
        }
        if ("18".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_18(aMandBer);
        }
        if ("20".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_20(aMandBer, aDeckArt);
        }
        if ("22".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_22(aMandBer);
        }
        if ("30".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_30(aMandBer);
        }
        if ("31".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_31(aMandBer);
        }
        if ("32".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_32(aMandBer);
        }
        if ("##".equals(aRechPostenArt)) {
            return mapSAPVerwendungsart_HashHash(aMandBer);
        }

        return mapSAPVerwendungsart_NotThere(aMandBer, aDeckArt);

    }

    private static String mapSAPVerwendungsart_01_02(
            final String aMandBer,
            final String aDeckForm,
            final Integer aDeckArt,
            final Integer aDeckungApgnr,
            final Integer aDeckungFanr) {

        if ("R".equals(aDeckForm)) {
            if ("G".equals(aMandBer)) {
                return "022";
            }
            if ("B".equals(aMandBer)) {
                return "222";
            }
        }
        if ("E".equals(aDeckForm) || "K".equals(aDeckForm)) {
            if ("G".equals(aMandBer)) {
                return "042";
            }
            if ("B".equals(aMandBer)) {
                return "242";
            }
            if ("S".equals(aMandBer) && aDeckArt != null && aDeckArt == 0) {
                return "242";
            }
        }
        if (aDeckArt != null && aDeckArt == 0 && aDeckForm == null) {
            if ("S".equals(aMandBer)) {
                return "612";
            }
            if ("B".equals(aMandBer)) {
                return "242";
            }
            if ("G".equals(aMandBer)) {
                return "042";
            }
        }
        if (aDeckArt != null && aDeckArt == 13) {
            if ("G".equals(aMandBer)) {
                return "062";
            }
            if ("B".equals(aMandBer)) {
                return "262";
            }
        }
        if (aDeckArt != null && aDeckArt == 50 && "S".equals(aMandBer)) {
            return "512";
        }
        if (aDeckArt != null && aDeckArt == 51 && "S".equals(aMandBer)) {
            return "612";
        }
        if (aDeckArt != null && aDeckArt == 12) {
            if ("G".equals(aMandBer)) {
                return "072";
            }
            if ("B".equals(aMandBer)) {
                return "272";
            }
        }
        if (aDeckungApgnr != null && aDeckungApgnr > 0 && aDeckungFanr != null && aDeckungFanr == 0) {
            return "112";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_03(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "091";
        }
        if ("B".equals(aMandBer)) {
            return "291";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_04(final String aMandBer, final Integer aDeckArt, final String aDeckForm) {
        if (aDeckArt != null && aDeckArt == 50 && "S".equals(aMandBer)) {
            return "501";
        }
        if (aDeckArt != null && aDeckArt == 51 && "S".equals(aMandBer)) {
            return "601";
        }
        if (aDeckArt != null && aDeckArt == 0 && "S".equals(aMandBer)) {
            if (aDeckForm == null) {
                return "601";
            }
            if ("R".equals(aDeckForm)) {
                return "301";
            }
        }
        if ("G".equals(aMandBer)) {
            return "101";
        }
        if ("B".equals(aMandBer)) {
            return "301";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_05(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "121";
        }
        if ("B".equals(aMandBer)) {
            return "321";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_06(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "131";
        }
        if ("B".equals(aMandBer)) {
            return "331";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_08(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "151";
        }
        if ("B".equals(aMandBer)) {
            return "351";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_13(final String aMandBer, final String aDeckForm) {
        if ("E".equals(aDeckForm) || "K".equals(aDeckForm)) {
            if ("G".equals(aMandBer)) {
                return "042";
            }
            if ("B".equals(aMandBer)) {
                return "242";
            }
        } else if ("R".equals(aDeckForm)) {
            if ("G".equals(aMandBer)) {
                return "022";
            }
            if ("B".equals(aMandBer)) {
                return "222";
            }
        }
        return null;
    }

    private static String mapSAPVerwendungsart_10_to_17_No_13(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "042";
        }
        if ("B".equals(aMandBer)) {
            return "242";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_18(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "162";
        }
        if ("B".equals(aMandBer)) {
            return "362";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_20(final String aMandBer, final Integer aDeckArt) {
        if (aDeckArt != null && aDeckArt == 10) {
            if ("G".equals(aMandBer)) {
                return "052";
            }
            if ("B".equals(aMandBer)) {
                return "252";
            }
            return null;
        }
        return null;
    }

    private static String mapSAPVerwendungsart_22(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "082";
        }
        if ("B".equals(aMandBer)) {
            return "282";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_30(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "181";
        }
        if ("B".equals(aMandBer)) {
            return "381";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_31(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "172";
        }
        if ("B".equals(aMandBer)) {
            return "372";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_32(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "192";
        }
        if ("B".equals(aMandBer)) {
            return "392";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_NotThere(final String aMandBer, final Integer aDeckArt) {
        if ("S".equals(aMandBer) && aDeckArt != null && aDeckArt == 51) {
            return "601";
        }
        if ("G".equals(aMandBer)) {
            return "163";
        }
        if ("B".equals(aMandBer)) {
            return "363";
        }
        return null;
    }

    private static String mapSAPVerwendungsart_HashHash(final String aMandBer) {
        if ("G".equals(aMandBer)) {
            return "990";
        }
        if ("B".equals(aMandBer)) {
            return "992";
        }
        if ("S".equals(aMandBer)) {
            return "995";
        }
        return null;
    }

    private static boolean is_postArt_either_10_11_12_14_15_16_17(final String aRechPostenArt) {
        return "10".equals(aRechPostenArt) || "11".equals(aRechPostenArt) || "12".equals(aRechPostenArt)
                || "14".equals(aRechPostenArt) || "15".equals(aRechPostenArt) || "16".equals(aRechPostenArt)
                || "17".equals(aRechPostenArt);
    }
}
