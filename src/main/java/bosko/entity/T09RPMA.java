package bosko.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "T09RPMA", schema = "AGA")
public class T09RPMA implements Serializable {

    private static final long serialVersionUID = 7190919296321175035L;

    private T09RPMAId t09rpmaId;

    private Integer updz;
    private BigDecimal mahn_geb_dm;
    private Integer sachb_aend;
    private Date aend_dat;
    private Integer nomination_kz;
    private String waehr_schl;

    private T09RPRE t09rpre;

    public T09RPMA() {

    }

    public T09RPMA(final T09RPRE aT09rpre, final Date aMahn_dat, final Integer aMahn_stufe) {
        setT09rpmaId(new T09RPMAId());
        getT09rpmaId().setMahn_dat(aMahn_dat);
        getT09rpmaId().setMahn_stufe(aMahn_stufe);
        aT09rpre.addT09rpmas(this);
    }

    @MapsId("t09rpreId")
    @JoinColumns({ @JoinColumn(name = "rechnr"), @JoinColumn(name = "rech_vorg_lfdnr") })
    @ManyToOne
    public T09RPRE getT09rpre() {
        return t09rpre;
    }

    public void setT09rpre(final T09RPRE aT09rpre) {
        t09rpre = aT09rpre;
    }

    @EmbeddedId
    public T09RPMAId getT09rpmaId() {
        return t09rpmaId;
    }

    public void setT09rpmaId(final T09RPMAId aT09rpmaId) {
        t09rpmaId = aT09rpmaId;
    }

    public Integer getUpdz() {
        return updz;
    }

    public void setUpdz(final Integer aUpdz) {
        updz = aUpdz;
    }

    @Column(precision = 18, scale = 2)
    public BigDecimal getMahn_geb_dm() {
        return mahn_geb_dm;
    }

    public void setMahn_geb_dm(final BigDecimal aMahn_geb_dm) {
        mahn_geb_dm = aMahn_geb_dm;
    }

    public Integer getSachb_aend() {
        return sachb_aend;
    }

    public void setSachb_aend(final Integer aSachb_aend) {
        sachb_aend = aSachb_aend;
    }

    public Date getAend_dat() {
        return aend_dat;
    }

    public void setAend_dat(final Date aAend_dat) {
        aend_dat = aAend_dat;
    }

    public Integer getNomination_kz() {
        return nomination_kz;
    }

    public void setNomination_kz(final Integer aNomination_kz) {
        nomination_kz = aNomination_kz;
    }

    public String getWaehr_schl() {
        return waehr_schl;
    }

    public void setWaehr_schl(final String aWaehr_schl) {
        waehr_schl = aWaehr_schl;
    }

}
