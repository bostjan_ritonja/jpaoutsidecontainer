package bosko.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import bosko.entity.T09RPRE;
import bosko.entity.T09RPREId;
import bosko.tools.SimpleConnectionTool;

public class Main {

    public static Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(final String[] args) {
        final Main tester = new Main();
        LOGGER.info("Testing JPA outside container");
        tester.testDb2Connection();
        tester.testWithOpenjpa();
    }

    private void testWithOpenjpa() {
        final EntityManager entityManager = SimpleConnectionTool.getEntityManager();
        LOGGER.info("Entity manager retreived.");
        LOGGER.info("Transaction started.");
        entityManager.getTransaction().begin();

        testLoadWithNativeQuery(entityManager);
        testLoadWithQuery(entityManager);

        entityManager.getTransaction().commit();
        entityManager.close();
        LOGGER.info("Transaction finished.");
    }

    private void testLoadWithQuery(final EntityManager aEntityManager) {
        final Query query = aEntityManager.createNamedQuery(T09RPRE.NamedQueries.EKG_ALL_T09RPRE_IDS);
        final List<T09RPREId> keys = query.getResultList();
        LOGGER.info("Retrived number of keys (T09RPREId) JPQL query: " + keys.size());

        final List<T09RPREId> keysForLoad = new ArrayList<T09RPREId>();
        keysForLoad.addAll(keys.subList(0, 4));

        testWithCriteriaBuilder(keysForLoad, aEntityManager);
        testLoadWitJPQL(keysForLoad, aEntityManager);
    }

    private void testWithCriteriaBuilder(final List<T09RPREId> aKeysForLoad, final EntityManager aEntityManager) {
        LOGGER.info("Loading objecta with Criteria builder");
        final List<T09RPRE> loadedObjects = T09RPRE.loadAllInWithCriteriaBuildedr(aEntityManager, aKeysForLoad);
        printLoadedObjects(loadedObjects);
        LOGGER.info("Objecta loaded with Criteria builder");
    }

    private void printLoadedObjects(final List<T09RPRE> aLoadedObjects) {
        for (final T09RPRE eachLoaded : aLoadedObjects) {
            LOGGER.info("Loaded object: " + eachLoaded);
            LOGGER.info("Loaded object id: " + eachLoaded.getT09rpreId().toString());
        }
    }

    @SuppressWarnings("unchecked")
    private void testLoadWitJPQL(final List<T09RPREId> aKeysForLoad, final EntityManager aEntityManager) {
        final Query query = aEntityManager.createNamedQuery(T09RPRE.NamedQueries.LOAD_FROM_LIST);
        query.setParameter("list", aKeysForLoad);
        LOGGER.info("Loading objecta with JPQL query");
        final List<T09RPRE> loadedObjects = query.getResultList();
        printLoadedObjects(loadedObjects);
        LOGGER.info("Objecta loaded with JPQL query");
    }

    @SuppressWarnings("unchecked")
    private void testLoadWithNativeQuery(final EntityManager aEntityManager) {

        final Query query = aEntityManager.createNativeQuery("select * from AGA.T09RPRE");
        final List<Object> result = query.getResultList();
        LOGGER.info("Retrived number of keys (T09RPREId) NATIVE query: " + result.size());
    }

    private void testDb2Connection() {
        Connection connection = null;
        try {
            connection = SimpleConnectionTool.getJDBC_Connection();
            LOGGER.info("JDBC Connection extablished: " + connection);
            connection.close();
        } catch (final SQLException e1) {
            e1.printStackTrace();
        }
    }

}
