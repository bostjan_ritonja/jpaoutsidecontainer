package bosko.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SimpleConnectionTool {

    private static final String PERSISTENCE_UNIT = "EkgPersistenceUnit";

    public static Connection getJDBC_Connection() throws SQLException {
        final String url = "jdbc:db2://localhost:50000/DBEN00";
        final String userid = "db2admin";
        final String passwd = "!A123456";
        final Connection con = DriverManager.getConnection(url, userid, passwd);
        return con;
    }

    public static EntityManager getEntityManager() {
        final Properties properties = getConnectionProperties();

        final EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory(PERSISTENCE_UNIT, properties);
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager;
    }

    private static Properties getConnectionProperties() {
        // NOTE: this properties are not needed since persistence.xml exists
        final Properties properties = new Properties();
        // properties.put("javax.persistence.jdbc.driver", "com.ibm.db2.jcc.DB2Driver");
        // properties.put("openjpa.ConnectionURL", "jdbc:db2://localhost:50000/DBEN00");
        // properties.put("openjpa.ConnectionUserName", "db2admin");
        // properties.put("openjpa.ConnectionPassword", "!A123456");
        // properties.put("openjpa.Log", "SQL=TRACE");
        return properties;
    }

}
