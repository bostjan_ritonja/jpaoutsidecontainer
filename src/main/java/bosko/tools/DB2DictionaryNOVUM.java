package bosko.tools;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.openjpa.jdbc.sql.DB2Dictionary;

public class DB2DictionaryNOVUM extends DB2Dictionary {

    public DB2DictionaryNOVUM() {
        maxColumnNameLength = 128;
    }

    @Override
    public String getString(final ResultSet rs, final int col) throws SQLException {
        String str = super.getString(rs, col);
        if (str != null) {
            str = str.trim();
        }
        return str;
    }

}
